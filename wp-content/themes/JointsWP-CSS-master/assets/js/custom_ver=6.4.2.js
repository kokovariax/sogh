//fade in menu on page load

jQuery(window).load(function(){
  jQuery("#menu-main-1").fadeIn("fast").css('display: block;');
});

// radio button functions on donate form

jQuery(document).ready(function() {
   jQuery('.page-template-page-donate #choice_3_33_1').click(function() {
       jQuery('.page-template-page-donate #field_3_31').hide();
   });
   jQuery('.page-template-page-donate #choice_3_33_0').click(function() {
       jQuery('.page-template-page-donate #field_3_31').show();
   });
   jQuery('.page-template-page-donate #choice_3_5_6').click(function() {
       jQuery('.page-template-page-donate #field_3_31').hide();
       jQuery('.page-template-page-donate #field_3_29').show();
   });

});
